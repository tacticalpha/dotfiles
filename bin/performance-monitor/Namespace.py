
class Namespace:
    def __init__(self, *args, **kwargs):
        if len(args) == 1 and type(args[0]) == dict:
            kwargs = {**args[0], **kwargs}

        for k in kwargs:
            setattr(self, k, kwargs[k])

    def __getitem__(self, k):
        return getattr(self, str(k))

    def __setitem__(self, k, v):
        setattr(self, str(k), v)

    def __iter__(self):
        return filter(lambda k: not k.startswith('_'), dir(self))

    def __repr__(self):
        return f'Namespace({", ".join([ f"{k}={self[k]}" for k in self ])})'

    @staticmethod
    def apply(target, *args):
        if target is None:
            target = Namespace()

        for arg in [target, *args]:
            if type(arg) not in [dict, Namespace]:
                raise ArgumentError('all Namespace.apply arguments must be either <dict> or <Namespace>')

        n = len(args)
        if n == 0:
            return target

        for arg in args:
            for k in arg:
                target[k] = arg[k]


        return target
