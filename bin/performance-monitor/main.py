
from dataclasses import dataclass

import argparse
import psutil
import math
import time
import sys

from Namespace import Namespace

HISTORY_SIZE = 100

@dataclass
class HistoryEntry:
  time: float
  value: any

def mean(list):
  return math.fsum(list) / len(list)

class Metric:
  def __init__(self, name, getter, *, unit=None, min_interval=0, 
               aggregator=None, aggregation_count=None, precision=(0,0),
               range=100):
    self.name   = name
    self.getter = getter

    self.unit              = unit
    self.min_interval      = min_interval
    self.aggregator        = aggregator
    self.aggregation_count = aggregation_count
    self.precision         = precision
    self.range             = range

    # self.value = None
    self.last_update = 0

    self.history = [None] * HISTORY_SIZE

  def update(self, *, force=False):
    now = time.time()
    if now - self.last_update < self.min_interval:
      return False
    
    value = self.getter()

    self.history.append(value)
    self.history.pop(0)

  def value(self):
    if self.aggregator:
      if not self.aggregation_count:
        raise ValueError('missing aggregation_count')
      
      return self.aggregator(self.history[-self.aggregation_count:])
    else:
      return self.history[-1]

metrics = [
  Metric('CPU', psutil.cpu_percent, unit='%', precision=(3,0)),
  Metric('Memory', lambda: psutil.virtual_memory().percent, unit='%', precision=(3,0)),
]

class obj():
  pass

esc = Namespace(
  inverse = '\033[7m',
  inverse_off = '\033[27m',
  alt = '\033[?1049h',
  alt_off = '\033[?1049l',
  clear = '\033[J',

  cursor = Namespace(
    home = '\033[H',
    goto = lambda column, line: f'\033[{line};{column}H\033[{line};{column}f',
  )
)


def progress_bar(progress, *, width=30):
  width_filled = math.floor(width * progress)
  width_empty = width - width_filled

  return f'[{esc.inverse}{" " * width_filled}{esc.inverse_off}{" " * width_empty}]'

def loop(*, args, longest_name):
  start = time.time()

  output = esc.cursor.home
  for metric in metrics:
    metric.update()

    if metric.precision[1]:
      specifier = f'0{sum(metric.precision) + 1}.{metric.precision[1]}f'
    else:
      specifier = f'0{metric.precision[0]}.0f'

    output += f'{metric.name:>{longest_name}}'
    output += ' '
    output += progress_bar(metric.value() / metric.range)
    output += ' '
    output += f'{metric.value():{specifier}}'
    output += ' '
    output += metric.unit
    output += '\n'

    # output += f'{metric.name}{metric.value():{specifier}}{metric.unit}\n'

  end = time.time()

  if args.debug:
    output += '\n'
    output += f'Metrics time: {end - start:01.3f}s'

  sys.stdout.write(output)
  sys.stdout.flush()

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-d', '--debug', action='store_true')
  args = parser.parse_args(sys.argv[1:])

  longest_name = 0
  for metric in metrics:
    if (length := len(metric.name)) > longest_name:
      longest_name = length

  try:
    sys.stdout.write(esc.alt + esc.clear)
    while True:
      loop(args=args, longest_name=longest_name)
      time.sleep(0.1)
  except KeyboardInterrupt:
    pass
  finally:
    sys.stdout.write(esc.alt_off)

if __name__ == '__main__':
  main()