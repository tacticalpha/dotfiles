#!/usr/bin/fish

set ticbox_root ( status current-filename | path resolve | path dirname )
set compose_services
for service in $ticbox_root/compose/*/
  if not test -e "$service/compose.yaml" -a -e "$service/service"
    continue
  end

  set -a compose_services ( path basename -- "$service" )
end

if test -t 1 # isatty stdout
  set s_normal ( set_color normal )
  set s_bold ( tput bold 2> /dev/null )
  set s_good ( set_color green )
  set s_neutral ( set_color blue )
  set s_bad  ( set_color brred )
end

function ticbox--completion
  set commands \
      "completion:output completion script for fish" \
      "down:stop service" \
      "help:display help" \
      "up:start service" \
      "status: Show status of all ticbox services"

  set commands_verbs
  for command in $commands
      set -a commands_verbs ( string split -f 1 ":" -- "$command" )
  end

  set script
  for command in $commands
      set command ( string split ":" -- "$command" )
      set -a script \
          "-n 'not __fish_seen_subcommand_from $commands_verbs' -a '$command[1]' -d '$command[2]'"
  end

  set -a script "-n '__fish_seen_subcommand_from status' -a 'compose'" 
  set -a script "-n '__fish_seen_subcommand_from up down' -a '$compose_services'" 

  set script ( string replace --regex -- '(.*)' 'complete -xc ticbox $1' $script )
  string join \n $script
end

function ticbox--status-compose
  docker ps &> /dev/null
  or return 1

  for service in $compose_services    
    set service_root "$ticbox_root/compose/$service"
    set service_config "$service_root/compose.yaml"

    set -l service_port
    set -l service_services 1
    for attribute in ( cat $service_config | string match --regex '^# ticbox_(?:\w+):\s*(?:.+)$' )
      string match --quiet --regex '^# ticbox_(?<name>\w+):\s*(?<value>.+)$' -- "$attribute"
      if not contains "$name" port services
        continue
      end
      set "service_$name" "$value"
    end

    if not $service_root/service ps &> /dev/null
      continue
    end

    set running_services ( $service_root/service ps )
    set running_services $running_services[2..]
    set running_services_n ( count $running_services )

    if test -n "$service_port"
      set port " ($service_port)"
    else
      set port ""
    end

    echo -n "$service$s_normal$port: $s_bold"\t
    if test $running_services_n -eq 0
      echo "$s_neutral""down""$s_normal"
    else if test $running_services_n -eq "$service_services"
      echo "$s_good""up""$s_normal"
    else
      echo "$s_bad""degraded ($running_services_n/$service_services)""$s_normal"
    end
  end
end

function ticbox--status
  if set -q argv[1]
    switch $argv[1]
      case "compose"
        ticbox--status-compose
      case "*"
        echo "unknown status target: '$argv[1]'" > /dev/stderr
        return 1
    end
    return 0
  end

  echo "$s_bold=== Compose ===$s_normal"
  ticbox--status-compose
end

function main
  # cd $ticbox_root

  set verb $argv[1]
  set argv $argv[2..]

  switch $verb
    case "completion" 
      ticbox--completion $argv

    case "status"
      ticbox--status $argv

    case "up" "down"
      set service $argv[1]
      set argv $argv[2..]

      if not contains "$service" $compose_services
        echo "unknown service '$service'"
        return 1
      end

      set service_root $ticbox_root/compose/$service/

      switch $verb
        case "up"
          $service_root/service up -d
        case "*"
          $service_root/service $verb
      end
          
  end
end

main $argv
return $status