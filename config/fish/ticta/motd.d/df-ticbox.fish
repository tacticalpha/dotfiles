
function ticta--motd--df-ticbox
  if not command -qv ticbox
    return 0
  end
  
  set s_normal ( set_color normal )
  set s_bold ( tput bold 2> /dev/null )
  set s_good ( set_color green )
  set s_neutral ( set_color blue )
  set s_bad  ( set_color brred )

  set status_compose ( ticbox status compose )

  set count_up ( string match --regex '^.*:\s*up' -- $status_compose | count )
  set count_degraded ( string match --regex '^.*:\s*degraded.*$' -- $status_compose | count )
  set count_down ( string match --regex '^.*:\s*down$' -- $status_compose | count )

  if test "$count_up" -eq 0 -a "$count_degraded" -eq 0
    return 0
  end

  set message "ticbox services running:"
  set -a message "$bold$s_good$count_up$s_normal up,"
  set -a message "$bold$s_bad$count_degraded$s_normal degraded,"
  set -a message "$bold$s_neutral$count_down$s_normal down"
  
  if test "$count_degraded" -gt 0
    echo "issue"
  else
    echo "session"
  end

  string join " " -- $message

end