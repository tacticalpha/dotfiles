
function ticta--motd--df-dotfiles
  set dim ( tput dim )
  set color ( set_color '#c781dc' )
  set normal ( set_color normal )
  
  set style_parenthesis "$normal$dim"
  set style_version "$normal$color"
  
  set start_pwd $PWD
  cd ( status current-filename | path resolve | path dirname )

  if not git status &> /dev/null
    echo "warn"
    echo "df-dotfiles motd part: not a git repo: $PWD"
    return 0
  end

  set branch ( git rev-parse --abbrev-ref HEAD )
  set local ( git rev-parse HEAD | string sub --length 7 )
  set remote ( git rev-parse origin/$branch 2> /dev/null | string sub --length 7 )
  or set local_branch
  git show --no-patch --format=%ci HEAD | read local_date local_time

  cd $start_pwd

  set message "dotfiles"
  if functions -q ticta--motd--ticta-release
    set -a message ""
  end
  set -a message " $style_version$local_date"
  set -a message "$style_parenthesis($style_version$local$style_parenthesis)$normal"

  if test "$branch" != "main"
    set -a message "on $style_version$branch"
  end

  if set -q local_branch
    set -a message "$dim(local branch)$normal"
  end

  echo "info"
  echo $message
end
