
function df--preferred
  set terminals \
    "alacritty" \
    "kitty" \
    "konsole" \
    "uxterm" \
    "xterm" 
  set editors micro nano vi
  set visuals code kate

  function df-editor--get-terminal
    if set -q PREFERRED_TERMINAL
      return
    end

    for terminal in $terminals
      if command -q "$terminal"
        set -gx PREFERRED_TERMINAL $terminal
        break
      end
    end
  end

  df-editor--get-terminal

  for editor in $editors
    if command -q "$editor"
      set -gx EDITOR "$editor"
      break
    end
  end

  # for visual in $visuals
  #   if command -q "$visual"
  #     set -gx VISUAL "$visual"
  #     break
  #   end
  # end
  set -eg VISUAL
end

df--preferred