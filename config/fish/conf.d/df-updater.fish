
function df--updater
  if test "$DOTFILE_FISH_UPDATER" != '1'
    exit 0
  end

  # not sure how I feel about this but I think it's ok
  set last_update "0$DOTFILE_LAST_UPDATE"
  set now ( date +%s )
  # set now 99999999999999
  set threshold ( math "1 * 60 * 60 * 18 " ) # 18h

  if test ( math "$now - $last_update" ) -gt "$threshold"
    set -U DOTFILE_LAST_UPDATE $now
    # echo "updating dotfiles in background. see log in $TMPDIR/dotfile-update.log"
    ~/.dotfiles/dotfiles update &> "$TMPDIR/dotfile-update.log" &
  end
end

df--updater