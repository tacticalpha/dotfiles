
function df--util-rmi
  if command -qv rmi
    return 1
  end

  function rmi
    set s_bold ( tput bold 2> /dev/null )
    set s_brred ( set_color brred )
    set s_normal ( set_color normal )

    for target in $argv
      echo "Remove '$target'?"
      df-ll -d "$target"
      if test -d "$target"
        set directories "?"
        set files "?"

        if not set directories ( timeout 1s find "$target" -type d )
          set directories "many"
        else
          set directories ( count $directories )

          if not set files ( timeout 1s find "$target" )
            set files "many"
          else
            set files ( math "$( count $files ) - $directories" )
          end
        end

        echo "-> contains $s_bold$s_brred$directories$normal directories"
        echo "            $s_bold$s_brred$files$normal files"
      end

      while read -P "(yes/[n]o/[t]ree/[l]ess) > " full_choice
        set choice ( string sub -l 1 -- "$full_choice" )

        if test "$full_choice" = "yes"
          break
        end

        switch $choice
          case "n"
            break
          case "t"
            df-lt "$target"
            continue
          case "l"
            if test -d "$target"
              df-lt --color always -- "$target" | less --raw-control-chars
            else
              less -- "$target"
            end
            continue
        end
      end

      if test "$full_choice" = "yes"
        rm -r "$target"
      end
    end
  end
end

df--util-rmi