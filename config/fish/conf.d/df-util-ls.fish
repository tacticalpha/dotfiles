
function df--util-ls
  set pre
  set opts
  set alias

  if command -q eza
    set target eza
    set alias "eza --group-directories-first"
  else if command -q exa
    set target exa
    set alias "exa --group-directories-first"
  else
    set pre "command"
    set target ls
    set opts "--color"
  end

  set cmd "$target"
  if test -n "$pre"
    set cmd "$pre $cmd"
  end
  if test -n "$opts"
    set cmd "$cmd $opts"
  end

  if test -n "$alias"
    alias "$target"="$alias"
  end

  abbr ls     "$cmd"
  alias df-ls "$cmd"
  abbr la     "$cmd -ag"
  alias df-la "$cmd -ag"
  abbr ll     "$cmd -aagl"
  alias df-ll "$cmd -aagl"

  if test "$target" = "ls"
    abbr lt     "$cmd -aal --recursive"
    alias df-lt "$cmd -aal --recursive"
  else
    abbr lt     "$cmd -a --tree --ignore-glob '.git'"
    alias df-lt "$cmd -a --tree --ignore-glob '.git'"
  end
end

df--util-ls